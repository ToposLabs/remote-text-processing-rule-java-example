FROM hronom/ubuntu1804-oraclejdk10

# Copy files
RUN mkdir application
COPY target/remote-text-processing-rule-java-example-*'[^stubs]'.jar /application/app.jar

WORKDIR /application

CMD ["./app.jar"]