# remote-text-processing-rule-java-example

To run locally, use:
```
EUREKA_URL={Eureka URL} docker-compose -f docker-compose.yml up -d
```

For example for dev launch(not inside docker) use `http://localhost:8102/eureka/`.

## Example requests
POST `/rule/process`
```
{
  "text": "Hello world",
  "language": "en",
  "config": {
    "addTextSize": true
  }
}
```