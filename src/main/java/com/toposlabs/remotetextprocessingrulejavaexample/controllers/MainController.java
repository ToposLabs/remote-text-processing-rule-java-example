package com.toposlabs.remotetextprocessingrulejavaexample.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kjetland.jackson.jsonSchema.JsonSchemaGenerator;
import com.toposlabs.remotetextprocessingrulejavaexample.components.TestRemoteConfig;
import com.toposlabs.remotetextprocessingrulejavaexample.controllers.pojo.info.RuleInfo;
import com.toposlabs.remotetextprocessingrulejavaexample.controllers.pojo.process.Annotation;
import com.toposlabs.remotetextprocessingrulejavaexample.controllers.pojo.process.ProcessRequest;
import com.toposlabs.remotetextprocessingrulejavaexample.controllers.pojo.process.ProcessResponse;

import net.moznion.random.string.RandomStringGenerator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;

@Validated
@RestController
public class MainController {
    private static final Logger logger = LogManager.getLogger();

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final JsonSchemaGenerator jsonSchemaGenerator = new JsonSchemaGenerator(objectMapper);

    private final RandomStringGenerator generator = new RandomStringGenerator();

    @GetMapping(path = "/rule/info", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public RuleInfo getInfo() {
        RuleInfo ruleInfo = new RuleInfo();
        ruleInfo.name = "Remote rule";
        ruleInfo.description = "This remote rule is example";
        ruleInfo.version = "1.0.0";
        ruleInfo.author = "Topos Labs";

        ruleInfo.includeContextFields = new HashSet<>();
        // Possible examples of values to include different context fields or all:
        ruleInfo.includeContextFields.add("all"); // This will include all fields.
        //ruleInfo.includeContextFields.add("language"); // Only language
        //ruleInfo.includeContextFields.add("timeIndex"); // Only timeIndex
        //ruleInfo.includeContextFields.add("tenseIndexes"); // Only tenseIndexes
        //ruleInfo.includeContextFields.add("sentimentIndex"); // Only sentimentIndex
        //ruleInfo.includeContextFields.add("datelineCandidate"); // Only datelineCandidate
        //ruleInfo.includeContextFields.add("dictionaryProximities"); // Only all dictionaryProximities
        //ruleInfo.includeContextFields.add("themeProximities"); // Only all themeProximities
        //ruleInfo.includeContextFields.add("doctypeProximities"); // Only all doctypeProximities
        //ruleInfo.includeContextFields.add("emotionProximities"); // Only all emotionProximities
        //ruleInfo.includeContextFields.add("geoEntities"); // Only all geoEntities
        //ruleInfo.includeContextFields.add("topicEntities"); // Only all topicEntities
        //ruleInfo.includeContextFields.add("annotationSet"); // Only full annotationSet
        //ruleInfo.includeContextFields.add("textRandomString"); // Only textRandomString in annotationSet

        ruleInfo.dependencies = new HashSet<>();
        ruleInfo.dependencies.add("TextParser");

        return ruleInfo;
    }

    @GetMapping(path = "/rule/config/schema", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Object getConfigSchema() {
        return jsonSchemaGenerator.generateJsonSchema(TestRemoteConfig.class);
    }

    @PostMapping(path = "/rule/config/schema", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public boolean validateConfig(@RequestBody Object config)
        throws JsonProcessingException {
        JsonNode jsonSchema = jsonSchemaGenerator.generateJsonSchema(TestRemoteConfig.class);
        String jsonSchemaAsString = objectMapper.writeValueAsString(jsonSchema);
        JSONObject rawSchema = new JSONObject(jsonSchemaAsString);
        Schema schema = SchemaLoader.load(rawSchema);
        try {
            String configJson = objectMapper.writeValueAsString(config);
            schema.validate(new JSONObject(configJson));
            return true;
        } catch (Exception exception){
            return false;
        }
    }

    @GetMapping(path = "/rule/config", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TestRemoteConfig getDefaultConfig() {
        return new TestRemoteConfig();
    }

    @PostMapping(path = "/rule/process", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ProcessResponse process(@RequestBody ProcessRequest processRequest)
        throws JsonProcessingException {

        logger.info(objectMapper.writeValueAsString(processRequest.context));

        TestRemoteConfig testRemoteConfig = objectMapper.convertValue(
            processRequest.config,
            TestRemoteConfig.class
        );

        StringBuilder finalPattern = new StringBuilder();
        for (int i = 0; i < processRequest.text.length(); i++) {
            char c = processRequest.text.charAt(i);
            if (Character.isLetter(c)) {
                if (Character.isUpperCase(c)) {
                    finalPattern.append("[A-Z]{1}");
                } else {
                    finalPattern.append("[a-z]{1}");
                }
            } else if (Character.isDigit(c)) {
                finalPattern.append("\\d");
            } else if (Character.isWhitespace(c)) {
                finalPattern.append(" ");
            } else {
                finalPattern.append("\\").append(c);
            }
        }

        ProcessResponse processResponse = new ProcessResponse();
        processResponse.annotationSet = new LinkedHashMap<>();
        {
            ArrayList<Annotation> annotations = new ArrayList<>();
            {
                Annotation annotation = new Annotation();
                annotation.start = 0;
                annotation.end = processRequest.text.length();
                annotation.text = processRequest.text;
                annotation.features = new LinkedHashMap<>();
                annotation.features.put(
                    "generatedString_keyword",
                    generator.generateByRegex(finalPattern.toString())
                );
                if (testRemoteConfig.addTextSize) {
                    annotation.features.put(
                        "textSize_integer",
                        processRequest.text.length()
                    );
                }
                annotations.add(annotation);
            }
            processResponse.annotationSet.put("textRandomString", annotations);
        }
        return processResponse;
    }
}
