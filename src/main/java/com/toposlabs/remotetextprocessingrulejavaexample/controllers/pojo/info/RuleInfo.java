package com.toposlabs.remotetextprocessingrulejavaexample.controllers.pojo.info;

import java.util.HashSet;

public final class RuleInfo {
    public String name;
    public String description;
    public String version;
    public String author;

    public HashSet<String> includeContextFields;
    public HashSet<String> dependencies;
}
