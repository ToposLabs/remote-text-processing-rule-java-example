package com.toposlabs.remotetextprocessingrulejavaexample.controllers.pojo.process.search;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SeDatelineEntityCandidate implements Serializable {
    public String dictionaryId;
    public String dictionaryName;
    public String entityId;
    public String mainName;
    public String weblink;
    public SeLocation location;
    public long population;
    public String feature;
}
