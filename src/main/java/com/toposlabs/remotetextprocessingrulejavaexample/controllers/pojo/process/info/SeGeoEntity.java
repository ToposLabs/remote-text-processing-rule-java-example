package com.toposlabs.remotetextprocessingrulejavaexample.controllers.pojo.process.info;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SeGeoEntity extends SeBaseEntity {
    public double latitude;
    public double longitude;
    public String featureClassDescription;
    public String featureCodeName;
    public int childrenNumber;
    public String[] childrenFeatureCodeNames;
    public SeGeoEntity parent;
    public List<SeGeoEntity> children;
}
