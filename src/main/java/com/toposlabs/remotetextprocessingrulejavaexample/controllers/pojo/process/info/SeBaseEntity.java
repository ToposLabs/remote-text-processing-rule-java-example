package com.toposlabs.remotetextprocessingrulejavaexample.controllers.pojo.process.info;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SeBaseEntity implements Serializable {
    public String id;
    public String info;
    public String mainName;
    public String mainWeblink;
    public String dictionaryId;
    public String dictionaryName;
}