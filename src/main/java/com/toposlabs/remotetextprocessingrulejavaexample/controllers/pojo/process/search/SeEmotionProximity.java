package com.toposlabs.remotetextprocessingrulejavaexample.controllers.pojo.process.search;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SeEmotionProximity implements Serializable {
    public String emotionId;
    public String emotionName;
    public double proximity;
}
