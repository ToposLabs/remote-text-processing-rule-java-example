package com.toposlabs.remotetextprocessingrulejavaexample.controllers.pojo.process.info;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SeRulePropertyDescription implements Serializable {
    /**
     * Name of property.
     */
    @JsonProperty
    public String name;
    /**
     * Description of property.
     */
    @JsonProperty
    public String description;
    /**
     * Default value of property.
     */
    @JsonProperty
    public String defaultValue;
}
