package com.toposlabs.remotetextprocessingrulejavaexample.controllers.pojo.process.search;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SeGeoEntityInText extends SeEntityInText {
    public SeLocation location;
    public long population;
    public String feature;
    public SeGeoEntityParent parents[];
}
