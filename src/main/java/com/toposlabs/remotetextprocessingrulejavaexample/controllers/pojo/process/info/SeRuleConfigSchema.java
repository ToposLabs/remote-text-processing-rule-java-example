package com.toposlabs.remotetextprocessingrulejavaexample.controllers.pojo.process.info;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.LinkedHashMap;

/**
 * Contains schema(description) of config used by rules in processing.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SeRuleConfigSchema implements Serializable {
    /**
     * Name of rule.
     */
    @JsonProperty
    public String name;
    /**
     * Config schema.
     */
    @JsonProperty
    public LinkedHashMap<String, Object> schema;
}
