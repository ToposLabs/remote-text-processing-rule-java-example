package com.toposlabs.remotetextprocessingrulejavaexample.controllers.pojo.process;

import java.util.LinkedHashMap;

public class Annotation {
    public int start;
    public int end;
    public String text;
    public LinkedHashMap<String, Object> features;
}