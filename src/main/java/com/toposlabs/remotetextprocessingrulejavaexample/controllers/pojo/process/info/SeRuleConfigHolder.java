package com.toposlabs.remotetextprocessingrulejavaexample.controllers.pojo.process.info;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SeRuleConfigHolder implements Serializable {
    @JsonProperty
    public String name;
    @JsonProperty
    public Object config;
}
