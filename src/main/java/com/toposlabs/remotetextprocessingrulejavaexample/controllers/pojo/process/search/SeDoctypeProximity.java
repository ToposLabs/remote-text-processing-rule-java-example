package com.toposlabs.remotetextprocessingrulejavaexample.controllers.pojo.process.search;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SeDoctypeProximity implements Serializable {
    public String doctypeId;
    public String doctypeName;
    public double proximity;
}
