package com.toposlabs.remotetextprocessingrulejavaexample.controllers.pojo.process;

import com.toposlabs.remotetextprocessingrulejavaexample.controllers.pojo.process.search.SeSearchResult;

public class ProcessRequest {
    public String text;
    public String language;
    public Object config;
    public SeSearchResult context;
}