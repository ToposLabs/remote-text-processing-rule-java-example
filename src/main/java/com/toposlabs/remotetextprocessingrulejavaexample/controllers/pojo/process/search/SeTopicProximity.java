package com.toposlabs.remotetextprocessingrulejavaexample.controllers.pojo.process.search;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SeTopicProximity implements Serializable {
    public String topicId;
    public String topicDictionaryName;
    public double proximity;
    public final ArrayList<SeTopicTypeProximity> topicTypeProximities = new ArrayList<>();
}
