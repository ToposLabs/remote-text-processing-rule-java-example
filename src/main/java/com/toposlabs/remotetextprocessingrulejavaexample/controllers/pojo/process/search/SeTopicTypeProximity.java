package com.toposlabs.remotetextprocessingrulejavaexample.controllers.pojo.process.search;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SeTopicTypeProximity implements Serializable {
    public String topicTypeId;
    public String topicTypeName;
    public double proximity;
}
