package com.toposlabs.remotetextprocessingrulejavaexample.controllers.pojo.process.search;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SeSearchResult implements Serializable {
    public SeLanguage language;
    /**
     * past/present/future of text.
     * [-1, 1] :
     * -1 - past,
     * 0 - present,
     * 1 - future.
     */
    public double timeIndex;
    /**
     * Array of three elements,
     * saying what part of text is composed in Past, Present, and Future tenses respectively.
     */
    public double[] tenseIndexes;
    /**
     * Index, describing sentiment of text:
     * positive(+1),
     * negative(-1),
     * or neutral(0).
     * Fractional numbers are allowed.
     */
    public double sentimentIndex;

    public SeDatelineEntityCandidate datelineCandidate;

    public final ArrayList<SeTopicProximity> dictionaryProximities = new ArrayList<>();
    public final ArrayList<SeBaseClassProximity> themeProximities = new ArrayList<>();
    public final ArrayList<SeBaseClassProximity> doctypeProximities = new ArrayList<>();
    public final ArrayList<SeBaseClassProximity> emotionProximities = new ArrayList<>();

    public final ArrayList<SeGeoEntityInText> geoEntities = new ArrayList<>();
    public final ArrayList<SeTopicEntityInText> topicEntities = new ArrayList<>();

    public final LinkedHashMap<String, ArrayList<SeAnnotation>> annotationSet = new LinkedHashMap<>();

    public final ArrayList<SeRuleLog> logging = new ArrayList<>();
}
