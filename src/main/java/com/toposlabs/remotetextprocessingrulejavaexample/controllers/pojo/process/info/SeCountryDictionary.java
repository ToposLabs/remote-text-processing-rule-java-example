package com.toposlabs.remotetextprocessingrulejavaexample.controllers.pojo.process.info;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SeCountryDictionary extends SeDictionary {
    public static final String noCountryCode = "--";
    public static final String noCountryName = "No country";

    /**
     * Continent
     */
    public String continent;
    /**
     * Capital of country.
     */
    public String capital;
    /**
     * Top level domain.
     */
    public String topLevelDomain;
    /**
     * Currency code.
     */
    public String currencyCode;
    /**
     * Currency name.
     */
    public String currencyName;
    /**
     * Phone code.
     */
    public String phoneCode;
    /**
     * Postal code format.
     */
    public String postalCodeFormat;
    /**
     * Postal code reg-exp.
     */
    public String postalCodeRegExp;
    /**
     * Can be null for no-country.
     */
    public SeGeoEntity entity;
}
