package com.toposlabs.remotetextprocessingrulejavaexample.controllers.pojo.process.search;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.LinkedHashMap;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SeAnnotation implements Serializable {
    public int start;
    public int end;
    public String text;
    public LinkedHashMap<String, Object> features;
}