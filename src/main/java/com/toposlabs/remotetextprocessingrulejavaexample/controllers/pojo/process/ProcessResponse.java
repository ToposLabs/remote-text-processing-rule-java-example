package com.toposlabs.remotetextprocessingrulejavaexample.controllers.pojo.process;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class ProcessResponse {
    public LinkedHashMap<String, ArrayList<Annotation>> annotationSet;
}