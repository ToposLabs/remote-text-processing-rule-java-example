package com.toposlabs.remotetextprocessingrulejavaexample.controllers.pojo.process.info;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SeDictionary implements Serializable {
    public String id;
    public String name;
    public String code;
}
