package com.toposlabs.remotetextprocessingrulejavaexample.controllers.pojo.process.info;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SeTopicDictionary extends SeDictionary {
    public final ArrayList<SeTopicType> seTopicTypes = new ArrayList<>();
}
