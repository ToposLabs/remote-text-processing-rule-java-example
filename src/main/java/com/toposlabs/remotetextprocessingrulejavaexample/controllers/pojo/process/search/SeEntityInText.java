package com.toposlabs.remotetextprocessingrulejavaexample.controllers.pojo.process.search;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class SeEntityInText implements Serializable {
    public String dictionaryId;
    public String dictionaryName;
    public String entityId;
    public String mainName;
    public String weblink;
    public double relevance;
    public double sentimentScore;
    public final ArrayList<SeLexemePosition> lexemePositions = new ArrayList<>();
}
