package com.toposlabs.remotetextprocessingrulejavaexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestRemoteRuleApp {
    public static void main(String[] args) {
        SpringApplication.run(TestRemoteRuleApp.class, args);
    }
}
