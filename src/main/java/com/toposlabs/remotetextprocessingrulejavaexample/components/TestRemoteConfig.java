package com.toposlabs.remotetextprocessingrulejavaexample.components;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kjetland.jackson.jsonSchema.annotations.JsonSchemaDefault;
import com.kjetland.jackson.jsonSchema.annotations.JsonSchemaDescription;
import com.kjetland.jackson.jsonSchema.annotations.JsonSchemaTitle;

public class TestRemoteConfig {
    @JsonProperty(required = true)
    @JsonSchemaTitle("Add string size")
    @JsonSchemaDescription("Need to add string size or not")
    @JsonSchemaDefault("false")
    public boolean addTextSize = false;
}